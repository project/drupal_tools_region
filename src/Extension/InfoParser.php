<?php

namespace Drupal\drupal_tools_region\Extension;

use Drupal\Core\Extension\InfoParser as CoreInfoParser;

/**
 * Class InfoParser.
 *
 * @package Drupal\drupal_tools_region\Extension
 */
class InfoParser extends CoreInfoParser {

  /**
   * The subject.
   *
   * @var \Drupal\Core\Extension\InfoParser
   */
  protected $subject;

  /**
   * InfoParser constructor.
   *
   * @param \Drupal\Core\Extension\InfoParser $subject
   *   The subject.
   * @param string|null $app_root
   *   The app root.
   */
  public function __construct(CoreInfoParser $subject, string $app_root = NULL) {
    parent::__construct($app_root);
    $this->subject = $subject;
  }

  /**
   * {@inheritdoc}
   */
  public function parse($filename) {
    $parsed_info = $this->subject->parse($filename);

    // Return if the file type isn't theme.
    if (!isset($parsed_info['type']) || $parsed_info['type'] !== 'theme') {
      return $parsed_info;
    }

    // Return if the theme no regions.
    if (!isset($parsed_info['regions'])) {
      return $parsed_info;
    }

    $parsed_info['regions']['drupal_tools'] = 'Drupal Tools';

    return $parsed_info;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredKeys() {
    return $this->subject->getRequiredKeys();
  }

}
