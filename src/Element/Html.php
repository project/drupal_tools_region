<?php

namespace Drupal\drupal_tools_region\Element;

use Drupal\Core\Render\Element\Html as CoreHtml;

/**
 * Class Html.
 *
 * @package Drupal\drupal_tools_region\Element
 */
class Html extends CoreHtml {

  /**
   * Builds the Drupal Tools as a structured array ready for drupal_render().
   *
   * @param array $element
   *   The element.
   *
   * @return array
   *   The element.
   */
  public static function preRenderDrupalTools($element) {
    if (!isset($element['page']['drupal_tools']) || !isset($element['page_bottom']['drupal_tools'])) {
      return $element;
    }

    $element['page_bottom']['drupal_tools'] = $element['page']['drupal_tools'];

    unset($element['page']['drupal_tools']);

    return $element;
  }

}
